<?php 
require_once("animal.php");
require_once("frog.php");
require_once("Ape.php");

$sheep = new Animal("shaun");

echo "Name : $sheep->name <br>"; // "shaun"
echo "Legs : $sheep->legs <br>"; // 4
echo "Cold Blooded: $sheep->cold_blooded <br>"; // false
echo "<br><br>";

$kodok = new Frog("buduk");
echo "Name : $kodok->name <br>"; // "shaun"
echo "Legs : $kodok->legs <br>"; // 4
echo "Cold Blooded : $kodok->cold_blooded <br>"; // false
echo $kodok->jump() ; // "hop hop"
echo "<br><br>";

$sungokong = new Ape("kera sakti");
echo "Name : $sungokong->name <br>"; // "shaun"
echo "Legs : $sungokong->legs <br>"; // 2
echo "Cold Blooded : $sungokong->cold_blooded <br>"; // false
$sungokong->yell(); // "Auooo"

?>